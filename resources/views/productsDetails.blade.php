 @extends('layouts.master')
 @section('title') Product Detalis @endsection
 @section('slider')
  <div class="container">
      <div class="page-banner">
        <div class="row justify-content-center align-items-center h-100">
          <div class="col-md-6">
            <nav aria-label="Breadcrumb">
              <ul class="breadcrumb justify-content-center py-0 bg-transparent">
                <li class="breadcrumb-item"><a href="{{ route('Home')}}">Home</a></li>
                <li class="breadcrumb-item active">Product</li>
              </ul>
            </nav>
            <h1 class="text-center">{{ $product->title}}</h1>
          </div>
        </div>
      </div>
    </div>
    @endsection
     @section('content')
   <div class="page-section">
    <div class="container">

      <div class="row my-5"> 
        <div class="col-lg-4 py-3">
          <div class="card-blog">
            <div class="header">
              <div class="post-thumb" style="height: 300px">
                <img src="{{ asset('public/product/'.$product->thumbnail)}}" alt="Product-Image" style="width: 100%;">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 py-3">
              <h4 class="post-title"><?=$product->title?></h4>
             <h6><?=$product->description?></h6>
             <div class="post-date">Price: <b>&#8377;</b><?=$product->price?></div>
            </div>
      </div>
    </div>
  </div>
  @endsection
  
 