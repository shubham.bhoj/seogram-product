 @extends('layouts.master')
 @section('title') Blog @endsection

    @section('content')
   <div class="page-section pt-5">
    <div class="container">
      <nav aria-label="Breadcrumb">
        <ul class="breadcrumb p-0 mb-0 bg-transparent">
          <li class="breadcrumb-item"><a href="{{ route('Home')}}">Home</a></li>
          <li class="breadcrumb-item"><a href="{{ route('Blog')}}">Blog</a></li>
          <li class="breadcrumb-item active">{{ $blog->title}}</li>
        </ul>
      </nav>
      
      <div class="row">
        <div class="col-lg-8">
          <div class="blog-single-wrap">
            <div class="header">
              <div class="post-thumb">
                <img src="{{ asset('public/blog-image/'.$blog->thumbnail)}}" alt="">
              </div>
              <div class="meta-header">
                <div class="post-author">
                  <div class="avatar">
                    <img src="{{ asset('public/assets/img/person/person_1.jpg')}}" alt="">
                  </div>
                  by <a href="#">{{$blog->User->name}}</a>
                </div>

                <!-- <div class="post-sharer">
                  <a href="#" class="btn social-facebook"><span class="mai-logo-facebook-f"></span></a>
                  <a href="#" class="btn social-twitter"><span class="mai-logo-twitter"></span></a>
                  <a href="#" class="btn social-linkedin"><span class="mai-logo-linkedin"></span></a>
                  <a href="#" class="btn"><span class="mai-mail"></span></a>
                </div> -->
              </div>
            </div>
            <h1 class="post-title">{{ $blog->title}}</h1>
            <div class="post-meta">
              <div class="post-date">
                <span class="icon">
                  <span class="mai-time-outline"></span>
                </span> <a href="#">{{ date("d M-Y", strtotime($blog->created_at));}}</a>
              </div>
              <div class="post-comment-count ml-2">
                <span class="icon">
                  <span class="mai-chatbubbles-outline"></span>
                </span> <a href="#">{{$blog->comment->count()}} Comments</a>
              </div>
            </div>
            <div class="post-content">
              <?=$blog->description?>
            </div>
          </div>

          <div class="comment-form-wrap pt-5">
            <h2 class="mb-5">Leave a comment</h2>
            <form action="{{ route('blogComment')}}" class="" method="post">
              @csrf
              <div class="form-row form-group">
                <div class="col-md-6">
                  <label for="name">Name *</label>
                  <input type="text" class="form-control" id="name" name="name">
                  @if ($errors->has('name'))
                  <span class="text-danger">{{ $errors->first('name') }}</span>
                  @endif
                  <input type="hidden" class="form-control" id="blog_id" name="blog_id" value="{{$blog->id}}">
                </div>
                <div class="col-md-6">
                  <label for="email">Email *</label>
                  <input type="email" class="form-control" id="email" name="email">
                  @if ($errors->has('email'))
                  <span class="text-danger">{{ $errors->first('email') }}</span>
                  @endif
                </div>
              </div>
              <!-- <div class="form-group">
                <label for="website">Website</label>
                <input type="url" class="form-control" id="website" name="website">
              </div> -->
  
              <div class="form-group">
                <label for="message">Message</label>
                <textarea name="msg" id="message" cols="30" rows="8" class="form-control" name="message"></textarea>
                 @if ($errors->has('msg'))
                  <span class="text-danger">{{ $errors->first('msg') }}</span>
                  @endif
              </div>
              <div class="form-group">
                <input type="submit" value="Post Comment" class="btn btn-primary">
              </div>
    
          @if(Session::has('success'))
          <div class="alert alert-success">
              {{ Session::get('success')}}
          </div>
         @endif
            </form>
          </div>

        </div>
        <div class="col-lg-4">
          <div class="widget">
            <!-- Widget search -->
            <div class="widget-box">
              <form action="{{route('Search')}}" class="search-widget" method="get">
                <input type="text" class="form-control" placeholder="Enter keyword.." name="search">
                <button type="submit" class="btn btn-primary btn-block">Search</button>
              </form>
               @if(Session::has('error'))
          <div class="alert alert-danger">
              {{ Session::get('error')}}
          </div>
         @endif

            </div>

            <!-- Widget Categories -->
            <div class="widget-box">
              <h4 class="widget-title">Category</h4>
              <div class="divider"></div>

              <ul class="categories">
                @foreach($categories as $category)
                <li><a href="{{ route('categoryFilter', $category->id)}}">{{ $category->name }}</a></li>
                @endforeach
              </ul>
            </div>

            <!-- Widget recent post -->
            <div class="widget-box">
              <h4 class="widget-title">Recent Post</h4>
              <div class="divider"></div>
              @foreach($recentBlogs as $blogs)
              <div class="blog-item">
                  <a class="post-thumb" href="">
                    <img src="{{ asset('blog-image/'.$blogs->thumbnail)}}" alt="">
                  </a>
                  <div class="content">
                    <h6 class="post-title"><a href="{{ route('blogDetail',$blogs->id)}}">{{ $blogs->title}}</a></h6>
                    <div class="meta">
                      <a href="#"><span class="mai-calendar"></span> {{ date("d M-Y", strtotime($blogs->created_at));}}</a>
                      <a href="#"><span class="mai-person"></span>{{$blogs->User->name}} </a>
                      <a href="#"><span class="mai-chatbubbles"></span> </a>
                    </div>
                  </div>
              </div>
              @endforeach
          

            </div>

            <!-- Widget Tag Cloud -->
            <div class="widget-box">
              <h4 class="widget-title">Tag Cloud</h4>
              <div class="divider"></div>

              <div class="tag-clouds">
                <a href="#" class="tag-cloud-link">Projects</a>
                <a href="#" class="tag-cloud-link">Design</a>
                <a href="#" class="tag-cloud-link">Travel</a>
                <a href="#" class="tag-cloud-link">Brand</a>
                <a href="#" class="tag-cloud-link">Trending</a>
                <a href="#" class="tag-cloud-link">Knowledge</a>
                <a href="#" class="tag-cloud-link">Food</a>
              </div>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>
  @endsection