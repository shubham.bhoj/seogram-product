<div class="sidebar">
            <nav class="sidebar-nav">
                <ul class="nav">
                    

                  

                    @if(Auth::user()->admin == true)
                    <li class="nav-title">Admin</li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminEnquiry')}}" class="nav-link {{ Route::currentRouteName() == 'adminEnquiry'?'active':''}}">
                            <i class="icon icon-speedometer"></i> Contact Enquiry
                        </a>                        
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminAbout')}}" class="nav-link {{ Route::currentRouteName() == 'adminAbout'?'active':''}}">
                            <i class="icon icon-doc"></i> About Us
                        </a>                        
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminServices')}}" class="nav-link {{ Route::currentRouteName() == 'adminServices'?'active':''}}">
                            <i class="icon icon-badge"></i> Services
                        </a>                        
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminSEOServices')}}" class="nav-link {{ Route::currentRouteName() == 'adminSEOServices'?'active':''}}">
                            <i class="icon icon-badge"></i> SEO Services
                        </a>                        
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminPlan')}}" class="nav-link {{ Route::currentRouteName() == 'adminPlan'?'active':''}}">
                            <i class="icon icon-paper-plane"></i> Pricing Plan
                        </a>                        
                    </li>

                    

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminCategory')}}" class="nav-link {{ Route::currentRouteName() == 'adminCategory'?'active':''}}">
                            <i class="icon icon-globe"></i> Categories
                        </a>                        
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminBlog')}}" class="nav-link {{ Route::currentRouteName() == 'adminBlog'?'active':''}}">
                            <i class="icon icon-layers"></i> Blog
                        </a>                        
                    </li>

                     <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminLink')}}" class="nav-link {{ Route::currentRouteName() == 'adminLink'?'active':''}}">
                            <i class="icon icon-link"></i> Social Link
                        </a>                        
                    </li>

                    <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminAddress')}}" class="nav-link {{ Route::currentRouteName() == 'adminAddress'?'active':''}}">
                            <i class="icon icon-notebook"></i> Address
                        </a>                        
                    </li>

                     <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminProducts')}}" class="nav-link {{ Route::currentRouteName() == 'adminProducts'?'active':''}}">
                            <i class="icon icon-globe"></i> Products
                        </a>                        
                    </li>

                     <li class="nav-item nav-dropdown">
                        <a href="{{ route('adminOrders')}}" class="nav-link {{ Route::currentRouteName() == 'adminOrders'?'active':''}}">
                            <i class="icon icon-list"></i> Orders
                        </a>                        
                    </li>

                  @else
                  <li class="nav-title">User</li>
                     <li class="nav-item">
                        <a href="{{ route('dashboard')}}" class="nav-link ">
                            <i class="icon icon-speedometer"></i> Dashboard
                        </a>
                    </li>
                   @endif
                   

                </ul>
            </nav>
        </div>