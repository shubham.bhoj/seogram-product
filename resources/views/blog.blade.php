 @extends('layouts.master')
 @section('title') Blog @endsection
 @section('slider')
 <div class="container">
      <div class="page-banner">
        <div class="row justify-content-center align-items-center h-100">
          <div class="col-md-6">
            <nav aria-label="Breadcrumb">
              <ul class="breadcrumb justify-content-center py-0 bg-transparent">
                <li class="breadcrumb-item"><a href="{{ route('Home')}}">Home</a></li>
                <li class="breadcrumb-item active">Blog</li>
              </ul>
            </nav>
            <h1 class="text-center">Blog</h1>
          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('content')
   <div class="page-section">
    <div class="container">
      <div class="row">
        <div class="col-sm-10">
          <form action="#" class="form-search-blog">
            <div class="input-group">
              <div class="input-group-prepend">
                <select id="categories" class="custom-select bg-light">
                  <option data-filter="all" value="All Categories">All Categories</option>
                  @foreach($categories as $category)
                  <option data-filter="cat-{{ $category->id }}" value="{{$category->name}}">{{$category->name}}</option>
                  @endforeach
                </select>
              </div>
              <input type="text" class="form-control" placeholder="Enter keyword.." id="filter">
            </div>
          </form>
        </div>
        <div class="col-sm-2 text-sm-right">
          <button class="btn btn-secondary">Filter <span class="mai-filter"></span></button>
        </div>
      </div>

      <div class="row my-5 blog">
        @foreach($blogs as $blog)
        <div class="col-lg-4 py-3" id="cat-{{$blog->category_id}}">
          <div class="card-blog">
            <div class="header">
              <div class="post-thumb">
                <img src="{{ asset('public/blog-image/'.$blog->thumbnail)}}" alt="">
              </div>
            </div>
            <div class="body">
              <h5 class="post-title"><a href="{{ route('blogDetail',$blog->id)}}"><?=$blog->title?></a></h5>
              <div class="post-date">Posted on <a href="#">{{ date("d M-Y", strtotime($blog->created_at));}}</a></div>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
      {{ $blogs->links('pagination::bootstrap-4') }}
    </div>
  </div>
  @endsection
  @section('script')
  <script>
  $("option").click(function(){
    var dataFilter = $(this).data('filter');
    $("#filter").val($(this).val());

    if(dataFilter == "all") {
      $(".blog .col-lg-4").show();
    }
    else
    {
      $(".blog .col-lg-4").hide();
      $(".blog #" + dataFilter).show();
    }
  });

</script>
  @endsection