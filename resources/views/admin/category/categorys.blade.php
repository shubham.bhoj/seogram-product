@extends('layouts.admin')

@section('title') Admin Category @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Categorys
   <a href="{{ route('adminNewCategory')}}" class="btn btn-primary">New Category</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sr. No</th>
                <th>Name</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($categories as $category)
            <tr>
                <td>{{ $i++;}}</td>
                <td>{{ $category->name }}</td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditCategory',$category->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
               <form action="{{ route('adminDeleteCategory',$category->id)}}" method="post" id="deleteproduct-{{ $category->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $category->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($categories as $category)
<div class="modal fade" id="deleteProductModal-{{ $category->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $category->name}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $category->id }}" action="{{ route('adminDeleteCategory', $category->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection