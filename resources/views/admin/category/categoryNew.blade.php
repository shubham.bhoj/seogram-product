@extends('layouts.admin')

@section('title')New Category @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               New Category
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('adminNewBlogCategory')}}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Category</label>
                            <input name="title" id="normal-input" class="form-control" placeholder="Category Name">
                        </div>
                    </div>
                </div>

                <div class="row">
               
                <button type="submit" class="btn btn-success"> Create Category</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection