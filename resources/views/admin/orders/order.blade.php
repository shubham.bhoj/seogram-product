@extends('layouts.admin')

@section('title') Admin Orders @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Orders List
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Order On</th>
                <th>Name</th>
                <th>Address</th>
                <th>Zip</th>
                <th>Payment Mode</th>
                <th>View List</th>
                <th>Order Status</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ date("d/M/Y", strtotime($order->created_at));}}</td>
                <td>{{ $order->name}}</td>
                
                <td><b>address</b>-{{ $order->address}},<br><b>city</b>-{{$order->city}},<b>state</b>-{{$order->state}}</td>
                <td>{{ $order->zip}}</td>
                <td>{{ $order->payment_type}}</td>
                <td><a class="btn btn-primary viewList" data-toggle="modal" data-target="#viewOrderModal-{{ $order->id }}" style="color:#fff;" data-id="{{$order->id}}">view</a></td>
                <td>{{ $order->status}}</td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditOrder',$order->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
               <form action="{{ route('adminDeleteOrder',$order->id)}}" method="post" id="deleteproduct-{{ $order->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $order->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($orders as $order)
<div class="modal fade" id="deleteProductModal-{{ $order->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $order->name}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $order->id }}" action="{{ route('adminDeleteOrder', $order->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

@foreach($orders as $order)
<div class="modal fade" id="viewOrderModal-{{ $order->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Order List.</h4>
        </div>
        <div class="modal-body">
            <div class="head"></div>
           <table class="table table-bordered product-purchase-list">
                <thead>
                    <tr>
                    <th>Sr No.</th>
                    <th>Image</th>
                    <th>Product</th>
                    <th>Quantity</th>
                </tr>
              </thead>
                <tbody class="showDetail">
                </tbody>
          </table>
        </div>
       
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection
@section('script')
<script type="text/javascript">
 $(".viewList").click(function(e){
    let id = $(this).attr('data-id');
    $.ajax({
        url: 'view-order/'+id,
        success:function(response){
            $(".head").html(response.head);
            $(".showDetail").html(response.data);
        },
        error:function(e){
            console.log(e)
        }

    })
 });

</script>

@endsection