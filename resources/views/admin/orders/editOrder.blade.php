@extends('layouts.admin')

@section('title')Edit Order @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               Edit Order
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
            	{{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
            	<ul>
            		@foreach($errors->all() as $error)
            		<li>
            			{{ $error}}
            		</li>
            		@endforeach
            	</ul>
            </div>
            @endif
            <form action="{{ route('adminEditOrderPost', $order->id)}}" method="post" enctype="multipart/form-data">
            	@csrf
            <div class="card-body">

            	 <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Name</label>
                            <input type="text" name="name" id="normal-input" class="form-control" placeholder="Name" value="{{ $order->name}}">
                        </div>
                    </div>
                   </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Email</label>
                            <input type="email" name="email" value="{{ $order->email}}" id="normal-input" class="form-control" placeholder="Email">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Address</label>
                            <input type="text" name="address" value="{{ $order->address}}" id="normal-input" class="form-control" placeholder="Address">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">City</label>
                            <input type="text" name="city" value="{{ $order->city}}" id="normal-input" class="form-control" placeholder="City">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">State</label>
                            <input type="text" name="state" value="{{ $order->state}}" id="normal-input" class="form-control" placeholder="State">
                        </div>
                    </div>
                     <div class="col-md-3">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Zip</label>
                            <input type="text" name="zip" value="{{ $order->zip}}" id="normal-input" class="form-control" placeholder="Zip">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Order Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="">Select Status</option>
                                <option value="pending" {{ $order->status == 'pending' ?'selected':''}}>Pending</option>
                                <option value="delivered" {{ $order->status == 'delivered' ?'selected':''}}>Delivered</option>
                                <option value="rejected" {{ $order->status == 'rejected' ?'selected':''}}>Rejected</option>
                            </select>
                            
                        </div>
                    </div>
                </div>
               
                <button type="submit" class="btn btn-success">Edit Order</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection