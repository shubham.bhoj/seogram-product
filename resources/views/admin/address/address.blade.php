@extends('layouts.admin')

@section('title') Admin Address @endsection

@section('content')
<style type="text/css">
  .cus-btn{
    background-color: #ef5350;
    border-radius: 10px;
    color: #fff;
    cursor: pointer;
   }
</style>

<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Address Bar
   <a href="{{ route('adminNewAddress')}}" class="btn btn-primary">New Address</a>
   <a href="{{ route('adminNewPhone')}}" class="btn btn-primary">New Phone</a>
   <a href="{{ route('adminNewEmail')}}" class="btn btn-primary">New Email Address</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-border">
            <thead>
            <tr>
                <th>Address</th>
                <th>Phone</th>
                <th>Email</th>
            </tr>
            
            </thead>
            <tbody>
            <?php $i = 1; ?>
           <tr>
            <td>
               @foreach($address as $addres)
               {{ $addres->address}}<form action="{{ route('adminDeleteAddress',$addres->id)}}" method="post" id="deleteAddress-{{ $addres->id }}" style="display: none">@csrf</form>
               <a><i class="icon icon-close cus-btn"  data-toggle="modal" data-target="#deleteAddressModal-{{ $addres->id }}"></i></a><br> 
               @endforeach
               </td>

               <td>@foreach($numbers as $num)
               {{ $num->number}}<form action="{{ route('adminDeletePhone',$num->id)}}" method="post" id="deleteNumber-{{ $num->id }}" style="display: none">@csrf</form>
               <i class="icon icon-close cus-btn"  data-toggle="modal" data-target="#deleteNumberModal-{{ $num->id }}"></i><br>
               @endforeach</td>
               

                <td>
               @foreach($emails as $email)
               {{ $email->email}}<form action="{{ route('adminDeleteEmail',$email->id)}}" method="post" id="deleteEmail-{{ $email->id }}" style="display: none">@csrf</form>
                <i class="icon icon-close cus-btn" data-toggle="modal" data-target="#deleteEmailModal-{{ $email->id }}"></i><br>
               @endforeach
               </td>
           </tr>

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($address as $addres)
<div class="modal fade" id="deleteAddressModal-{{ $addres->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $addres->address}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteAddress-{{ $addres->id }}" action="{{ route('adminDeleteAddress', $addres->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@foreach($numbers as $num)
<div class="modal fade" id="deleteNumberModal-{{ $num->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $num->number}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteNumber-{{ $num->id }}" action="{{ route('adminDeletePhone', $num->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

@foreach($emails as $email)
<div class="modal fade" id="deleteEmailModal-{{ $email->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $email->email}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteEmail-{{ $email->id }}" action="{{ route('adminDeleteEmail', $email->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach

 
@endsection