@extends('layouts.admin')

@section('title')New Email @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               New Email Address
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('adminPostEmail')}}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Email Address</label>
                            <input name="email" id="normal-input" class="form-control" placeholder="Email">
                        </div>
                    </div>
                </div>

                <div class="row">
               
                <button type="submit" class="btn btn-success"> Add Email</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection