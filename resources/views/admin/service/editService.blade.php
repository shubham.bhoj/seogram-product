@extends('layouts.admin')

@section('title')Edit Service @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               Edit Service
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
            	{{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
            	<ul>
            		@foreach($errors->all() as $error)
            		<li>
            			{{ $error}}
            		</li>
            		@endforeach
            	</ul>
            </div>
            @endif
            <form action="{{ route('adminEditPostService', $service->id)}}" method="post" enctype="multipart/form-data">
            	@csrf
            <div class="card-body">

            	 <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Thumbnail</label>
                            <input type="file" name="thumbnail" id="normal-input" class="form-control" placeholder="Service Thumbnail">
                            <input type="hidden" name="thumbnail_old" id="normal-input" class="form-control" value="<?=$service->thumbnail?>">
                        </div>
                    </div>
                   </div>
                    <img src="{{asset('public/services-image/'.$service->thumbnail)}}" width="100">
                

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Title</label>
                            <input name="title" value="{{ $service->title}}" id="normal-input" class="form-control" placeholder="Service Title">
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="placeholder-input" class="form-control-label">Description </label>
                           <textarea class="form-control" name="description" cols="30" rows="10" id="" placeholder="Service Description">{{ $service->description}}</textarea>
                        </div>
                    </div>
                </div>
               
                <button type="submit" class="btn btn-success">Update Service</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection