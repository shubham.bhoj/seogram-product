@extends('layouts.admin')

@section('title') Admin Product @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Blogs
   <a href="{{ route('adminNewProduct')}}" class="btn btn-primary">New Product</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sr. No</th>
                <th>Thumbnail</th>
                <th>Title</th>
                <th>Description</th>
                <th>Product in Stock</th>
                <th>Price</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
                <?php  $a= 1;?>
            @foreach($products as $product)
            <tr>
                <td>{{ $a++}}</td>
                <td><img src="{{ asset('public/product/'.$product->thumbnail)}}" height="100px"></td>
                <td class="text-nowrap"><a href="{{ route('adminEditProduct',$product->id)}}"> {{ $product->title}}</a></td>
                <td><?=Str::limit($product->description, 50) ?></td>
                <td>{{ $product->in_stock}}</td>
               <td>{{ $product->price}}</td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditProduct',$product->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>

               <form action="{{ route('adminDeleteProduct',$product->id)}}" method="post" id="deleteproduct-{{ $product->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $product->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($products as $product)
<div class="modal fade" id="deleteProductModal-{{ $product->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $product->title}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $product->id }}" action="{{ route('adminDeleteProduct', $product->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection