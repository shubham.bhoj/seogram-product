@extends('layouts.admin')

@section('title') Admin Pricing Plan @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Pricing Plan
   <a href="{{ route('adminNewPlan')}}" class="btn btn-primary">New Plan</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sr. No</th>
                <th>Template</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
            <?php $i = 1; ?>
            @foreach($plans as $plan)
            <tr>
                <td>{{ $i++;}}</td>
                <td><?=$plan->plan?></td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditPlan',$plan->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
               <form action="{{ route('adminDeletePlan',$plan->id)}}" method="post" id="deleteplan-{{ $plan->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletePlanModal-{{ $plan->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($plans as $plan)
<div class="modal fade" id="deletePlanModal-{{ $plan->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete plan.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deletePlan-{{ $plan->id }}" action="{{ route('adminDeletePlan', $plan->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection