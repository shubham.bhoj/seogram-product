@extends('layouts.admin')

@section('title')Edit Plan @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               Edit Plan
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('adminEditPostPlan',$plan->id)}}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body">

                <div class="row mt-4">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="placeholder-input" class="form-control-label">Plan </label>
                           <textarea class="form-control" name="description" id="" placeholder="Service Description">{{ $plan->plan}}</textarea>
                        </div>
                    </div>
                </div>
                <div class="row">
               
                <button type="submit" class="btn btn-success"> Add Plan</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection