@extends('layouts.admin')

@section('title') Admin About Us @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin About Us
   <a href="{{ route('adminNewAbout')}}" class="btn btn-primary">Add About Us</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>About Us</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
            @foreach($abouts as $about)
            <tr>
                <td>{{ $about->title}}</td>
                <td><img src="{{ asset('public/about-image/'.$about->thumbnail)}}" height="200px"></td>
                <td><?=$about->about_us?></td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditAbout',$about->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>
               <form action="{{ route('adminDeleteBlog',$about->id)}}" method="post" id="deleteproduct-{{ $about->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $about->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($abouts as $about)
<div class="modal fade" id="deleteProductModal-{{ $about->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ Str::limit($about->about_us, 10) }}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $about->id }}" action="{{ route('adminDeleteAbout', $about->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection