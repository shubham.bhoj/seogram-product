@extends('layouts.admin')

@section('title') Admin SEO Services @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin SEO Services
   <a href="{{ route('adminSEONewServices')}}" class="btn btn-primary">New SEO Service</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sr. No</th>
                <th>Thumbnail</th>
                <th>Title</th>
                <th>Description</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
            @foreach($services as $service)
            <tr>
                <td>{{ $service->id}}</td>
                <td><img src="{{ asset('public/SEOservices-image/'.$service->thumbnail)}}" width="100"></td>
                <td class="text-nowrap"><a href="{{ route('adminSEOEditService',$service->id)}}"> {{ $service->title}}</a></td>
                <td>{{ $service->description }}</td>
               
                <td style="display: inline-flex;">
               <a href="{{ route('adminSEOEditService',$service->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>

               <form action="{{ route('adminSEODeleteService',$service->id)}}" method="post" id="deleteproduct-{{ $service->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $service->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($services as $service)
<div class="modal fade" id="deleteProductModal-{{ $service->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $service->title}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $service->id }}" action="{{ route('adminSEODeleteService', $service->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection