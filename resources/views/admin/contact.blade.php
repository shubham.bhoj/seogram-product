@extends('layouts.admin')

@section('title') Contact List @endsection

@section('content')

<div class="content">
<div class="card">
<div class="card-header bg-light">
    Contact List
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>Subject</th>
                <th>Message</th>
                 <th>Date</th>
            </tr>
            
            </thead>
            <tbody>
                <?php $a = 1;?>
            @foreach($contacts as $contact)
            <tr>
                <td>{{ $a++ }}</td>
                <td class="text-nowrap"> {{ ucfirst($contact->first_name) }}  {{ ucfirst($contact->last_name )}}</td>
                <td>{{ $contact->email }}</td>
                <td>{{ $contact->subject }}</td>
                <td>{{ $contact->message }}</td>
                <td>{{ date("d/M/Y", strtotime($contact->created_at))}}</td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
</div>

@endsection