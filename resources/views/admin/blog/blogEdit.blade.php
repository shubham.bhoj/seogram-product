@extends('layouts.admin')

@section('title')Edit Blog @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               Edit Blog
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
            	{{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
            	<ul>
            		@foreach($errors->all() as $error)
            		<li>
            			{{ $error}}
            		</li>
            		@endforeach
            	</ul>
            </div>
            @endif
            <form action="{{ route('adminEditPostBlog', $blog->id)}}" method="post" enctype="multipart/form-data">
            	@csrf
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Category</label>
                            <select name="category" id="category" class="form-control">
                                <option value="">Select Category</option>
                                @foreach($categories as $category)
                                <option value="{{ $category->id}}" {{ $category->id == $blog->category->id ?'selected':''}}>{{ $category->name }}</option>
                                @endforeach
                            </select>
                            
                        </div>
                    </div>
                </div>

            	 <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Thumbnail</label>
                            <input type="file" name="thumbnail" id="normal-input" class="form-control" placeholder="Blog Thumbnail">
                            <input type="hidden" name="thumbnail_old" id="normal-input" class="form-control" value="<?=$blog->thumbnail?>">
                        </div>
                    </div>
                   </div>
                    <img src="{{asset('public/blog-image/'.$blog->thumbnail)}}" width="100">
                

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Title</label>
                            <input name="title" value="{{ $blog->title}}" id="normal-input" class="form-control" placeholder="Blog Title">
                        </div>
                    </div>
                </div>

                <div class="row mt-4">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="placeholder-input" class="form-control-label">Description </label>
                           <textarea class="form-control" name="description" cols="30" rows="10" id="" placeholder="Blog Description">{{ $blog->description}}</textarea>
                        </div>
                    </div>
                </div>
               
                <button type="submit" class="btn btn-success">Update Blog</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection