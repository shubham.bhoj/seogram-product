@extends('layouts.admin')

@section('title') Admin Blogs @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Blogs
   <a href="{{ route('adminNewBlog')}}" class="btn btn-primary">New Blog</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sr. No</th>
                <th>Category</th>
                <th>Thumbnail</th>
                <th>Title</th>
                <th>Description</th>
                <th>Post On</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
                <?php  $a= 1;?>
            @foreach($blogs as $blog)
            <tr>
                <td>{{ $a++}}</td>
                <td>{{ $blog->category->name}}</td>
                <td><img src="{{ asset('public/blog-image/'.$blog->thumbnail)}}" height="100px"></td>
                <td class="text-nowrap"><a href="{{ route('adminEditBlog',$blog->id)}}"> {{ $blog->title}}</a></td>
                <td>{{ Str::limit($blog->description, 50) }}</td>
               <td>{{ date("d/M/Y", strtotime($blog->created_at));}}</td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditBlog',$blog->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>

               <form action="{{ route('adminDeleteBlog',$blog->id)}}" method="post" id="deleteproduct-{{ $blog->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $blog->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($blogs as $blog)
<div class="modal fade" id="deleteProductModal-{{ $blog->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete {{ $blog->title}}.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $blog->id }}" action="{{ route('adminDeleteBlog', $blog->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection