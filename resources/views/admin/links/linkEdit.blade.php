@extends('layouts.admin')

@section('title')Edit Social Links @endsection

@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">
               Edit Social Links
            </div>
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{ Session::get('success')}}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                    <li>
                        {{ $error}}
                    </li>
                    @endforeach
                </ul>
            </div>
            @endif
            <form action="{{ route('adminEditPostLink',$link->id)}}" method="post" enctype="multipart/form-data">
                @csrf
            <div class="card-body">

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Facebook</label>
                            <input name="facebook" id="normal-input" class="form-control" placeholder="Facebook" value="{{ $link->facebook}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Twitter</label>
                            <input name="twitter" id="normal-input" class="form-control" placeholder="Twitter" value="{{ $link->twitter}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Google</label>
                            <input name="google" id="normal-input" class="form-control" placeholder="Google" value="{{ $link->google}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Instagram</label>
                            <input name="instagram" id="normal-input" class="form-control" placeholder="Instagram"value="{{ $link->instagram}}">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="normal-input" class="form-control-label">Youtube</label>
                            <input name="youtube" id="normal-input" class="form-control" placeholder="Youtube" value="{{ $link->youtube}}">
                        </div>
                    </div>
                </div>

                <div class="row">
               
                <button type="submit" class="btn btn-success"> Update Links</button>
            </div>
            </form>
        </div>
    </div>
</div>

</div>
</div>
@endsection