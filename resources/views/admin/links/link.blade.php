@extends('layouts.admin')

@section('title') Admin Social Links @endsection

@section('content')

	<div class="content">
<div class="card">
<div class="card-header bg-light">
   Admin Social Links
   <a href="{{ route('adminNewLink')}}" class="btn btn-primary">New Social link</a>
</div>

<div class="card-body">
    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Sr. No</th>
                <th>Facebook</th>
                <th>Twitter</th>
                <th>Google</th>
                <th>Instagram</th>
                <th>Youtube</th>
                <th>Actions</th>
            </tr>
            
            </thead>
            <tbody>
            </tbody>
            <?php $a = 1;?>
            @foreach($links as $link)
            <tr>
                <td>{{ $a++}}</td>
                <td>{{$link->facebook}}</td>
                <td>{{$link->twitter}}</td>
                <td>{{$link->google}}</td>
                <td>{{$link->instagram}}</td>
                <td>{{$link->youtube}}</td>
                <td style="display: inline-flex;">
               <a href="{{ route('adminEditLink',$link->id)}}" class="btn btn-warning"><i class="icon icon-pencil"></i></a>

               <form action="{{ route('adminDeleteLink',$link->id)}}" method="post" id="deleteproduct-{{ $link->id }}" style="display: none">@csrf</form>
               <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteProductModal-{{ $link->id }}">X</button>
                </td>
            </tr>
           @endforeach 

            </tbody>
        </table>
    </div>
</div>
</div>
@if(Session::has('success'))
<div class="alert alert-success">
    {{ Session::get('success')}}
</div>
@endif
</div>

@foreach($links as $link)
<div class="modal fade" id="deleteProductModal-{{ $link->id }}" role="dialog" tabindex="-1" aria-lablledby="myModalLabel">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Are you about to delete Link.</h4>
        </div>
        <div class="modal-body">
          <p>Are you sure?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">No, keep it</button>
          <form id="deleteProduct-{{ $link->id }}" action="{{ route('adminDeleteLink', $link->id)}}" method="post">@csrf
          <button type="submit" class="btn btn-primary">Yes, delete it</button>
          </form>
        </div>
      </div>
      
    </div>
  </div>
@endforeach
 
@endsection