 @extends('layouts.master')
 @section('title') Products @endsection
 @section('slider')
 <style type="text/css">
.counter{
 width: 35%;
 display: flex;
 justify-content: space-between;
 align-items: center;
 margin-left: 80px;
 margin-top: -10px;
}

.count{
 font-size: 17px;
 font-family: ‘Open Sans’;
 font-weight: 900;
 color: #787575;
}

.counter button {
  border: none;
  border-radius: 5px;
  outline: 0;
  padding: 7px;
  color: white;
  background-color: #6c55f9;
  text-align: center;
  cursor: pointer;
  width: 100%;
  font-size: 13px;
}
 </style>

  <div class="container">
      <div class="page-banner">
        <div class="row justify-content-center align-items-center h-100">
          <div class="col-md-6">
            <nav aria-label="Breadcrumb">
              <ul class="breadcrumb justify-content-center py-0 bg-transparent">
                <li class="breadcrumb-item"><a href="{{route('Home')}}">Home</a></li>
                <li class="breadcrumb-item active">Product</li>
              </ul>
            </nav>
            <h1 class="text-center">Products</h1>
          </div>
        </div>
      </div>
    </div>
    @endsection
     @section('content')
      
   <div class="page-section">
    <div class="container">
      @if(Session::has('message'))
       <div class="alert alert-danger">
         {{ Session::get('message')}}
       </div>
      @endif
      @if(Session::has('success'))
       <div class="alert alert-success">
         {{ Session::get('success')}}
       </div>
      @endif
      <div class="row my-5 blog">
    
        @foreach($products as $product)
        <div class="col-lg-4 py-3">
          <div class="card-blog">
            <div class="header">
              <div class="post-thumb">
                <a href="{{ route('productDetail',$product->id)}}"><img src="{{ asset('public/product/'.$product->thumbnail)}}" alt="Product-Image" style="width: 100%;"></a>
              </div>
            </div>
            <div class="row body">
              <h5 class="post-title"><a href="{{ route('productDetail',$product->id)}}"><?=$product->title?></a></h5>
             <h6><?= Str::limit($product->description, 100)?></h6>
             <div class="post-date">Price: <b>&#8377;</b><?=$product->price?></div>
             <div class="counter">
              @php $checkQty = \App\Models\Product::findorfail($product->id); @endphp
              @if($checkQty->in_stock <= 0)
               <button>Out Of stock</button>
               @else
              @if($cart->where('id',$product->id)->count())
              <button type="submit">In cart</button>
              @else
              <form action="{{ route('cartStore')}}" method="post">
                @csrf
                <input type="hidden" name="product_id" value="{{$product->id}}">
                <button type="submit">Add to Cart</button>
              </form>
              @endif
               @endif
            </div>
            
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
      <!--  -->
    </div>
  </div>
  @endsection
 