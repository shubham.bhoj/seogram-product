 @extends('layouts.master')
 @section('title') View Cart @endsection
 @section('slider')
 <style type="text/css">
   .row {
  display: -ms-flexbox; /* IE10 */
  display: flex;
  -ms-flex-wrap: wrap; /* IE10 */
  flex-wrap: wrap;
  margin: 0 -16px;
}

.col-25 {
  -ms-flex: 25%; /* IE10 */
  flex: 25%;
}

.col-50 {
  -ms-flex: 50%; /* IE10 */
  flex: 50%;
}

.col-75 {
  -ms-flex: 75%; /* IE10 */
  flex: 75%;
}

.col-25,
.col-50,
.col-75 {
  padding: 0 16px;
}

.container-product {
  background-color: #f2f2f2;
  padding: 5px 20px 15px 20px;
  border: 1px solid lightgrey;
  border-radius: 3px;
}

input[type=text],[type=email] {
  width: 100%;
  padding: 12px;
  margin-bottom: 20px;
  border: 1px solid #ccc;
  border-radius: 3px;
}

label {
  margin-bottom: 10px;
  display: block;
}

.icon-container {
  margin-bottom: 20px;
  padding: 7px 0;
  font-size: 24px;
}

.btn-checkout {
  padding: 12px;
  margin: 10px 0;
  border: none;
  width: 100%;
  border-radius: 3px;
  cursor: pointer;
  font-size: 17px;
}


span.qty {
  float: center;
  color: grey;
}

span.price {
  float: right;
  color: grey;
}


@media (max-width: 800px) {
  .row {
    flex-direction: column-reverse;
  }
  .col-25 {
    margin-bottom: 20px;
  }
}
 </style>
 <div class="page-section">
    <div class="container">
 <div class="row">
  <div class="col-75">
    <div class="container-product">
      <form method="post" action="{{ route('OrderPlaced')}}" >
        @csrf
        <div class="row">
          <div class="col-50">
            <h3>Shipping Address</h3>
            <br>
            <label for="fname"><i class="fa fa-user"></i> Full Name</label>
            <input type="text" id="fname" name="name" placeholder=" ">
            @if($errors->has('name'))
            <span class="text-danger" style="margin-bottom: 20px;">{{ $errors->first('name') }}</span> 
            @endif
            <label for="email"><i class="fa fa-envelope"></i> Email</label>
            <input type="email" id="email" name="email" placeholder=" ">
            @if($errors->has('email'))
            <span class="text-danger" style="margin-bottom: 20px;">{{ $errors->first('email') }}</span> 
            @endif
            <label for="adr"><i class="fa fa-address-card-o"></i> Address</label>
            <input type="text" id="adr" name="address" placeholder=" ">
            @if($errors->has('address'))
            <span class="text-danger" style="margin-bottom: 20px;">{{ $errors->first('address') }}</span> 
            @endif
            <label for="city"><i class="fa fa-institution"></i> City</label>
            <input type="text" id="city" name="city" placeholder=" ">
            @if($errors->has('city'))
            <span class="text-danger" style="margin-bottom: 20px;">{{ $errors->first('city') }}</span> 
            @endif
            <div class="row">
              <div class="col-50">
                <label for="state">State</label>
                <input type="text" id="state" name="state" placeholder=" ">
                @if($errors->has('state'))
                <span class="text-danger" style="margin-bottom: 20px;">{{ $errors->first('state') }}</span> 
                @endif
              </div>
              <div class="col-50">
                <label for="zip">Zip</label>
                <input type="text" id="zip" name="zip" placeholder=" ">
                @if($errors->has('zip'))
                <span class="text-danger" style="margin-bottom: 20px;">{{ $errors->first('zip') }}</span> 
                @endif
              </div>
            </div>
          </div>

          <div class="col-50">
            <h3>Payment Method</h3>
            <br>
           <!--  <label for="fname">Accepted Cards</label>
            <div class="icon-container">
              <i class="fa fa-cc-visa" style="color:navy;"></i>
              <i class="fa fa-cc-amex" style="color:blue;"></i>
              <i class="fa fa-cc-mastercard" style="color:red;"></i>
              <i class="fa fa-cc-discover" style="color:orange;"></i>
            </div>
            <label for="cname">Name on Card</label>
            <input type="text" id="cname" name="cardname" placeholder=" ">
            <label for="ccnum">Credit card number</label>
            <input type="text" id="ccnum" name="cardnumber" placeholder=" ">
            <label for="expmonth">Exp Month</label>
            <input type="text" id="expmonth" name="expmonth" placeholder=" "> -->

            <div class="row">
              <div class="col-50">
                 @foreach( \Gloudemans\Shoppingcart\Facades\Cart::content() as $product)
                 <input type="hidden" name="product_id[]" value="{{ $product->id }}">
                 <input type="hidden" name="qty[]" value="{{ $product->qty }}">
                 @endforeach
                <input type="radio" id="cod" name="cod" value="cod" checked> Cash on delivery
              </div>
            </div>
          </div>
        </div>
        <input type="submit" value="Continue to checkout" class="btn btn-primary btn-checkout">
      </form>
    </div>
  </div>

  <div class="col-25">
    <div class="container-product">
      <h4>Cart</h4>
     @foreach( \Gloudemans\Shoppingcart\Facades\Cart::content() as $product)
      <p><a href="#">{{ $product->name}}</a> <span class="price">&#8377;{{ $product->price}}x{{ $product->qty }}</span></p>
     @endforeach
      <hr>
      <p>Total <span class="price" style="color:black"><b>&#8377;</b>{{ Cart::subtotal()}}</span></p>
    </div>
  </div>
</div> 
</div>
</div>
  @endsection