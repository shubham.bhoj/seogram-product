 @extends('layouts.master')
 @section('title') Blog @endsection
 @section('slider')
 <div class="container">
      <div class="page-banner">
        <div class="row justify-content-center align-items-center h-100">
          <div class="col-md-6">
            <nav aria-label="Breadcrumb">
              <ul class="breadcrumb justify-content-center py-0 bg-transparent">
                <li class="breadcrumb-item"><a href="{{ route('Home')}}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('Blog')}}">Blog</a></li>
                <li class="breadcrumb-item active">Category</li>
              </ul>
            </nav>
            <h1 class="text-center">{{$categoryName->name}}</h1>
          </div>
        </div>
      </div>
    </div>
    @endsection
    @section('content')
   <div class="page-section">
    <div class="container">
      <div class="row my-5 blog">
        @foreach($blogs as $blog)
        <div class="col-lg-4 py-3" id="cat-{{$blog->category_id}}">
          <div class="card-blog">
            <div class="header">
              <div class="post-thumb">
                <img src="{{ asset('public/blog-image/'.$blog->thumbnail)}}" alt="">
              </div>
            </div>
            <div class="body">
              <h5 class="post-title"><a href="{{ route('blogDetail',$blog->id)}}"><?=$blog->title?></a></h5>
              <div class="post-date">Posted on <a href="#">{{ date("d M-Y", strtotime($blog->created_at));}}</a></div>
            </div>
          </div>
        </div>
        @endforeach
        
      </div>
    </div>
  </div>
  @endsection
  