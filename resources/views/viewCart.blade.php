 @extends('layouts.master')
 @section('title') View Cart @endsection
 @section('slider')
 <style type="text/css">
.counter{
 width: 20%;
 display: flex;
 justify-content: space-between;
 align-items: center;
 margin-left: -20px;
 padding-top: 10px;
}

.count{
 font-size: 17px;
 font-family: ‘Open Sans’;
 font-weight: 900;
 color: #787575;
}
.total{
  height: 35px;
}


 </style>
  <div class="container">
      <div class="page-banner">
        <div class="row justify-content-center align-items-center h-100">
          <div class="col-md-6">
            <nav aria-label="Breadcrumb">
              <ul class="breadcrumb justify-content-center py-0 bg-transparent">
                <li class="breadcrumb-item"><a href="{{ route('Home')}}">Home</a></li>
                <li class="breadcrumb-item active">View Cart</li>
              </ul>
            </nav>
            <h1 class="text-center">Cart View</h1>
          </div>
        </div>
      </div>
    </div>
    @endsection
     @section('content')
     
   <div class="page-section">
    <div class="container">
      @if(Session::has('message'))
             <div class="alert alert-danger">
               {{ Session::get('message')}}
             </div>
      @endif
      @foreach( \Gloudemans\Shoppingcart\Facades\Cart::content() as $product)
      <div class="row my-5"> 
        <div class="col-lg-4 py-3">
          <div class="card-blog">
            <div class="header">
              <div class="post-thumb" style="height: auto;">
                @php $image = \App\Models\Product::findorfail($product->id); @endphp
                <img src="{{ asset('public/product/'.$image->thumbnail)}}" alt="Product-Image">
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 py-3">
              <h4 class="post-title">{{$product->name}}</h4>
             <div class="post-date">Price: <b>&#8377;</b>{{($product->price*$product->qty) }}</div>
             <h6 class="post-title">{{$product->description}}</h6>
             <div class="counter">
              <div class="add btn">
                <form method="post" action="{{ route('addCart',$product->rowId)}}">
                @csrf
                <input type="hidden" name="pid" value="{{ $product->id}}">
                <input type="hidden" name="addQty" value="{{ $product->qty}}">
                <button type="submit">+</button>
              </form>
            </div>
              <div class="count">{{$product->qty}}</div>
              <div class="btn mins">
                <form method="post" action="{{ route('minsCart',$product->rowId)}}">
                @csrf
                <input type="hidden" name="minsQty" value="{{ $product->qty}}">
                <button type="submit">-</button>
              </form>
              </div>
              </div>
        </div>
      </div>
       @endforeach
       <div class="row my-5">
        <div class="col-lg-6 py-3"></div>
         <div class="col-lg-4 py-3" style="text-align: right;">
             <div class="total"><b>Total: </b>{{Cart::subtotal()}}</div>
             <div><a class="btn btn-primary" href="{{ route('checkOut') }}">proccess to pay</a></div>
          </div>
   </div>
 </div>
</div>
  @endsection

  
 