<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','PublicController@index')->name('Home');
Route::get('about','PublicController@about')->name('About');
Route::get('service','PublicController@service')->name('Service');
Route::get('blogs','PublicController@blog')->name('Blog');
Route::get('blog-details/{id}','PublicController@blogDetail')->name('blogDetail');
Route::post('comment','PublicController@blogComment')->name('blogComment');
Route::get('contact','PublicController@contact')->name('Contact');
Route::post('contact','PublicController@contactPost')->name('contactPost');
Route::post('newslater','PublicController@newsLater')->name('newsLater');
Route::get('category/{id}','PublicController@categoryFilter')->name('categoryFilter');
Route::get('search','PublicController@Search')->name('Search');


Route::get('products','ProductController@Products')->name('Products');
Route::get('product-detali/{product}','ProductController@productsDetails')->name('productDetail');
Route::post('add-to-cart','ProductController@addtoCart')->name('cartStore');
Route::get('view-cart','ProductController@viewCart')->name('ViewCart');
Route::post('add-cart/{rowId}','ProductController@addCart')->name('addCart');
Route::post('mins-cart/{rowId}','ProductController@minsCart')->name('minsCart');
Route::get('checkout-cart','ProductController@checkOutCart')->name('checkOut');
Route::post('order-place','ProductController@OrderPlaced')->name('OrderPlaced');


Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::prefix('admin')->group(function(){
    Route::get('dashboard','AdminController@index')->name('adminDashboard');
    Route::get('contact','AdminController@getContact')->name('adminEnquiry');

    Route::get('about','AdminController@about')->name('adminAbout');
    Route::get('about/new','AdminController@newAbout')->name('adminNewAbout');
    Route::post('about/new','AdminController@newPostAbout')->name('adminNewPostAbout');
    Route::get('about/edit/{about}','AdminController@editAbout')->name('adminEditAbout');
    Route::post('about/edit/{id}','AdminController@editPostAbout')->name('adminEditPostAbout');
    Route::post('about/delete/{id}','AdminController@deleteAbout')->name('adminDeleteAbout');

    Route::get('services','AdminController@services')->name('adminServices');
    Route::get('services/new','AdminController@newServices')->name('adminNewServices');
    Route::post('services/new','AdminController@newPostServices')->name('adminNewServicesPost');
    Route::get('service/edit/{service}','AdminController@editServices')->name('adminEditService');
    Route::post('service/edit/{id}','AdminController@editPostServices')->name('adminEditPostService');
    Route::post('service/delete/{id}','AdminController@deleteService')->name('adminDeleteService');

    Route::get('services/SEO','AdminController@servicesSEO')->name('adminSEOServices');
    Route::get('services/SEO/new','AdminController@newServicesSEO')->name('adminSEONewServices');
    Route::post('services/SEO/new','AdminController@newPostServicesSEO')->name('adminSEONewServicesPost');
    Route::get('service/SEO/edit/{service}','AdminController@editServicesSEO')->name('adminSEOEditService');
    Route::post('service/SEO/edit/{id}','AdminController@editPostServicesSEO')->name('adminSEOEditPostService');
    Route::post('service/SEO/delete/{id}','AdminController@deleteServiceSEO')->name('adminSEODeleteService');

    Route::get('plan','AdminController@plan')->name('adminPlan');
    Route::get('plan/new','AdminController@newPlan')->name('adminNewPlan');
    Route::post('plan/new','AdminController@newPostPlan')->name('adminNewPlanPost');
    Route::get('plan/edit/{plan}','AdminController@editPlan')->name('adminEditPlan');
    Route::post('plan/edit/{id}','AdminController@editPostPlan')->name('adminEditPostPlan');
    Route::post('plan/delete/{id}','AdminController@deletePlan')->name('adminDeletePlan');


    Route::get('blog','AdminController@blog')->name('adminBlog');
    Route::get('blog/new','AdminController@newBlog')->name('adminNewBlog');
    Route::post('blog/new','AdminController@newPostBlog')->name('adminNewBlogPost');
    Route::get('blog/edit/{blog}','AdminController@editBlog')->name('adminEditBlog');
    Route::post('blog/edit/{id}','AdminController@editPostBlog')->name('adminEditPostBlog');
    Route::post('blog/delete/{id}','AdminController@deleteBlog')->name('adminDeleteBlog');


    Route::get('category','AdminController@category')->name('adminCategory');
    Route::get('category/new','AdminController@newCategory')->name('adminNewCategory');
    Route::post('category/new','AdminController@newPostCategory')->name('adminNewBlogCategory');
    Route::get('category/edit/{category}','AdminController@editCategory')->name('adminEditCategory');
    Route::post('category/edit/{id}','AdminController@editPostCategory')->name('adminEditPostCategory');
    Route::post('category/delete/{id}','AdminController@deleteCategory')->name('adminDeleteCategory');


    Route::get('socialLinks','AdminController@links')->name('adminLink');
    Route::get('socialLinks/new','AdminController@newLinks')->name('adminNewLink');
    Route::post('socialLinks/new','AdminController@newPostLinks')->name('adminNewPostLink');
    Route::get('socialLinks/edit/{link}','AdminController@editLinks')->name('adminEditLink');
    Route::post('socialLinks/edit/{id}','AdminController@editPostLinks')->name('adminEditPostLink');
    Route::post('socialLinks/delete/{id}','AdminController@deleteLinks')->name('adminDeleteLink');

    Route::get('address','AdminController@address')->name('adminAddress');
    Route::get('address/new','AdminController@newAddress')->name('adminNewAddress');
    Route::post('address/new','AdminController@newPostAddress')->name('adminNewAddress');
    Route::post('address/delete/{id}','AdminController@deleteAddress')->name('adminDeleteAddress');

    Route::get('phone/new','AdminController@phone')->name('adminNewPhone');
    Route::post('phone/new','AdminController@newPostPhone')->name('adminPostPhone');
    Route::post('phone/delete/{id}','AdminController@deletePhone')->name('adminDeletePhone');

    Route::get('email/new','AdminController@email')->name('adminNewEmail');
    Route::post('email/new','AdminController@newPostEmail')->name('adminPostEmail');
    Route::post('email/delete/{id}','AdminController@deleteEmail')->name('adminDeleteEmail');

    Route::get('products','AdminController@products')->name('adminProducts');
    Route::get('product/new','AdminController@newProduct')->name('adminNewProduct');
    Route::post('product/new','AdminController@newPostProduct')->name('adminNewPostProduct');
    Route::get('product/edit/{product}','AdminController@editProduct')->name('adminEditProduct');
    Route::post('product/edit/{id}','AdminController@editPostProduct')->name('adminEditPostProduct');
    Route::post('product/delete/{id}','AdminController@deleteProduct')->name('adminDeleteProduct');

    Route::get('orders','AdminController@orders')->name('adminOrders');
    Route::get('view-order/{id}','AdminController@viewOrder')->name('viewOrder');
    Route::get('order/edit/{order}','AdminController@editOrder')->name('adminEditOrder');
    Route::post('order/edit/{id}','AdminController@editOrderPost')->name('adminEditOrderPost');
    Route::post('order/delete/{id}','AdminController@deleteOrder')->name('adminDeleteOrder');

    });