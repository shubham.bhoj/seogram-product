<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;

    public function Category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    public function User()
    {
        return $this->belongsTo('App\Models\User');
    }

     public function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
