<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use Illuminate\Support\collect;
use Gloudemans\Shoppingcart\Facades\Cart;

class ProductController extends Controller
{
    public function Products()
    {
        $products = Product::all();
        $cart = Cart::content();
        return view('products',compact('products','cart'));
    }

    public function productsDetails(Product $product)
    {
        return view('productsDetails',compact('product'));
    }

    public function addtoCart(Request $request)
    {
        $product = Product::findorFail($request->product_id);
        Cart::add($product->id,$product->title,1,$product->price);
        return redirect()->route('Products');
    }

    public function viewCart(Request $request)
    {
        if(Cart::content()->count() > 0){
         return view('viewCart');
        }else{
            return redirect()->route('Products')->with('message','Please Add Product into cart before view-cart.');
        }
    }

    public function addCart(Request $request,$rowId)
    {
         $product_qty = Product::findorFail($request->pid);
         $update_qty = $request->addQty+1;
         if($update_qty <= $product_qty->in_stock){
         Cart::update($rowId,$update_qty);
         return redirect()->route('ViewCart');
         }else{
             return redirect()->route('ViewCart')->with('message','Can\'t Add quantity for '.$product_qty->title);
         }
    }
    public function minsCart(Request $request,$rowId)
    {
         Cart::update($rowId,$request->minsQty-1);
         return redirect()->route('ViewCart');
    }

    public function checkOutCart(Request $request)
    {
        return view('checkOutCart');
    }

    public function OrderPlaced(Request $request)
    {

        $this->validate($request,[
        'name' => 'required|string|max:255',
        'email' => 'required|email',
        'address' => 'required|string',
        'city' => 'required|string',
        'state' => 'required',
        'zip'  => 'required',
        'cod' => 'required'
        ]);
        if($request->product_id[0] != null){
            $address = new DeliveryAddress();
            $address->name = $request->name;
            $address->email = $request->email;
            $address->address = $request->address;
            $address->city = $request->city;
            $address->state = $request->state;
            $address->zip = $request->zip;
            $address->payment_type = $request->cod;
            if($address->save()){
                for($i=0;$i<count($request->product_id);$i++){
                  $orders = new Order();  
                  $orders->order_id = $address->id;
                  $orders->product_id = $request->product_id[$i];
                  $orders->qty = $request->qty[$i];
                  if($orders->save())
                  {
                    $quantity = Product::findorFail($request->product_id[$i]);
                    $quantity->in_stock = $quantity->in_stock - $request->qty[$i];
                    $quantity->save();
                  }
                }
                Cart::destroy();    
                return redirect()->route('Products')->with('success','Order Placed Successfully.');
            }
       }else{
        return redirect()->route('Products')->with('message','There is something wrong. Please try again.');
       }
    }

}
