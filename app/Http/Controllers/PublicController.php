<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Blog;
use App\Models\Plan;
use App\Models\About;
use App\Models\Email;
use App\Models\Number;
use App\Models\Address;
use App\Models\Comment;
use App\Models\Contact;
use App\Models\Service;
use App\Models\Category;
use App\Models\Newslatter;
use App\Models\SEOService;
use Illuminate\Http\Request;
use App\Http\Requests\CommentRequest;
use App\Http\Requests\ContactRequest;

class PublicController extends Controller
{
    public function index()
    {
        $service = Service::paginate(3);
        $SEOService = SEOService::all();
        $plans = Plan::all();
        $about = About::first();
        $blogs= Blog::paginate(3);
        return view('index',compact('service','SEOService','plans','about','blogs'));
    }

    public function about()
    {   
        $about = About::first();
        $plans = Plan::all();
        return view('about',compact('plans','about'));
    }

    public function service()
    {
        $services = Service::all();
        $SEOServices = SEOService::all();
        return view('service',compact('services','SEOServices'));
    }

     public function blog()
    {
        $blogs = Blog::paginate(9);
        $categories = Category::all();
        return view('blog',compact('blogs','categories'));
    }

    public function blogDetail($id)
    {
        $blog = Blog::findorFail($id);
        $categories = Category::all();
        $recentBlogs = Blog::orderBy('id', 'desc')->paginate(3);
        return view('blogDetail',compact('blog','categories','recentBlogs'));
    }

    public function blogComment(CommentRequest $request)
    {

        $comment = new Comment;
        $comment->blog_id = $request['blog_id'];
        $comment->name = $request['name'];
        $comment->email = $request['email'];
        $comment->message = $request['msg'];
        $comment->save();
        return redirect()->to(route('blogDetail',$request['blog_id']))->with('success','Thank you your feetback.');
    }


    public function contact()
    {
        $address = Address::all();
        $numbers = Number::all();
        $emails = Email::all();
        return view('contact',compact('address','numbers','emails'));
    }

    public function contactPost(ContactRequest $request)
    {
        
        $contact = new Contact;  
        $contact->first_name = $request['first_name'];
        $contact->last_name = $request['last_name'];
        $contact->email = $request['email'];
        $contact->subject = $request['subject'];
        $contact->message = $request['message'];
        $contact->save();
        return redirect()->to(route('Contact'))->with('success','Thank you for contact us. We will connect you soon.');
    }

    public function newsLater(Request $request)
    {
        $this->validate($request,[
            'emailAddress' => 'required|email|unique:newslatters,email'],
        );
        $newslater = new Newslatter;
        $newslater->email = $request['emailAddress'];
        $newslater->save();
        return back()->with('success','Thank you for Subscribe us. We will connect you soon.');

    }
    public function categoryFilter($id)
    {
        $blogs = Blog::where('category_id',$id)->get();
        $categoryName = Category::findorFail($id);
        $categories = Category::all();
        return view('categoryBlog',compact('blogs','categories','categoryName'));
    }

    public function Search(Request $request)
    {
        $search = $request['search'];
        if($search != ""){
        $category = Category::where('name', 'LIKE', '%' . $search . '%')->first();
        if($category != ""){
        $blogs = Blog::where('category_id',$category->id)->get();
        return view('search',compact('category','blogs'));
        }
        return back()->with('error' , 'Can\'t find any category.Please try another Keyword.');
        }else{
            return back()->with('error' , 'Can\'t find any category.Please try another Keyword.');
        }
    }
      
}
