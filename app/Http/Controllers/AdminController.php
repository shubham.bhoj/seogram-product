<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Plan;
use App\Models\User;
use App\Models\About;
use App\Models\Email;
use App\Models\Order;
use App\Models\Number;
use App\Models\Address;
use App\Models\Contact;
use App\Models\Product;
use App\Models\Service;
use App\Models\Category;
use App\Models\SEOService;
use App\Models\SocialLink;
use Illuminate\Http\Request;
use App\Models\DeliveryAddress;
use App\Http\Requests\BlogRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ServiceRequest;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('admin.dashboard');
    }

    public function getContact()
    {
        $contacts = Contact::orderBy('id','desc')->get();
        return view('admin.contact',compact('contacts'));
    }



// -------------------- About Section --------------------
    public function about()
    {
        $abouts = About::all();
        return view('admin.about.about',compact('abouts'));
    }

    public function newAbout()
    {
        return view('admin.about.aboutNew');
    }

    public function newPostAbout( Request $request)
    {
        $this->validate($request,[
        'title' => 'required|string|max:255',
        'thumbnail' => 'required|file',
        'description' => 'required'
        ]);

        $about = new About;
        $about->title = $request['title'];
        $about->about_us = $request['description'];
        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        $thumbnail->move('public/about-image', $filename);

         $about->thumbnail = $filename;
         $about->save(); 
          return redirect()->to(route('adminAbout'))->with('success','About Us Add Successfully.');
    }

    public function editAbout(About $about)
    {
        return view('admin.about.aboutEdit',compact('about'));
    }

    public function editPostAbout(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required|string|max:255',
            'description' => 'required'
        ]);

        $about = About::findorFail($id);
        $about->title = $request['title'];
        $about->about_us = $request['description'];
        $old_image = $request['thumbnail_old'];

        if($request->hasFile('thumbnail'))
        {
        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        if($thumbnail->move('public/about-image', $filename))
        {
            @unlink('public/about-image/'.$old_image);
        }
        $about->thumbnail = $filename;
        }
        
         $about->save(); 
         return redirect()->to(route('adminAbout'))->with('success','About Us Edit Successfully.');
    }

    public function deleteAbout($id)
    {
        $about = About::findorFail($id);
        if($about)
        {
            unlink('public/about-image/'.$about->thumbnail);
            $about->delete();
        }
        return redirect()->to(route('adminAbout'))->with('success','About Us Delete Successfully.');
    }



    // -------------------- Services Section --------------------



    public function services()
    {
        $services = Service::all();
        return view('admin.service.services',compact('services'));
    }

    public function newServices()
    {
        return view('admin.service.newServices');
    }

    public function newPostServices(ServiceRequest $request)
    {
        $service = new Service;
        $service->title = $request['title'];
        $service->description = $request['description'];

        $thumbnail = $request->file('thumbnail');
        $filename = $thumbnail->getClientOriginalName();
        $thumbnail->move('public/services-image', $filename);

         $service->thumbnail = $filename;
         $service->save(); 
          return redirect()->to(route('adminServices'))->with('success','Service Add Successfully.');
    }

    public function editServices(Service $service)
    {
        return view('admin.service.editService',compact('service'));
    }

    public function editPostServices(Request $request, $id)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'description' => 'required',
        ]);

        $service = Service::findorFail($id);
        $service->title = $request['title'];
        $service->description = $request['description'];
        $old_image = $request['thumbnail_old'];

        if($request->hasFile('thumbnail'))
        {
        $thumbnail = $request->file('thumbnail');
        $filename = $thumbnail->getClientOriginalName();
        if($thumbnail->move('services-image', $filename))
        {
            @unlink('public/services-image/'.$old_image);
        }
        $service->thumbnail = $filename;
        }
        
         $service->save(); 
         return redirect()->to(route('adminServices'))->with('success','Service Edit Successfully.');
    }

    public function deleteService($id)
    {
        $service = Service::findorFail($id);
        if($service){
        unlink('public/services-image/'.$service->thumbnail);
        $service->delete();
        }
        return redirect()->to(route('adminServices'))->with('success','Service Delete Successfully.');
        
    }


      // -------------------- SEo Services Section --------------------

    public function servicesSEO()
    {
        $services = SEOService::all();
        return view('admin.seoService.SEOservice',compact('services'));
    }

    public function newServicesSEO()
    {
        return view('admin.seoService.SEONewservice');
    }

    public function newPostServicesSEO(ServiceRequest $request)
    {
         $service = new SEOService;
        $service->title = $request['title'];
        $service->description = $request['description'];

        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        $thumbnail->move('public/SEOservices-image', $filename);
         $service->thumbnail = $filename;
         $service->save(); 
          return redirect()->to(route('adminSEOServices'))->with('success','SEO Service Add Successfully.');
    }

    public function editServicesSEO(SEOService $service)
    {
        return view('admin.seoService.editSEOService',compact('service'));
    }

    public function editPostServicesSEO(Request $request,$id)
    {
        $this->validate($request,[
            'title' => 'required|string',
            'description' => 'required',
        ]);

        $service = SEOService::findorFail($id);
        $service->title = $request['title'];
        $service->description = $request['description'];
        $old_image = $request['thumbnail_old'];

        if($request->hasFile('thumbnail'))
        {
        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        if($thumbnail->move('public/SEOservices-image', $filename))
        {
            @unlink('public/SEOservices-image/'.$old_image);
        }
        $service->thumbnail = $filename;
        }
        
         $service->save(); 
         return redirect()->to(route('adminSEOServices'))->with('success','SEO Service Edit Successfully.');
    }

    public function deleteServiceSEO($id)
    {
        $service = SEOService::findorFail($id);
        if($service)
        {
            @unlink('public/SEOServices-image/'.$service->thumbnail);
            $service->delete();
        }
        return redirect()->to(route('adminSEOServices'))->with('success','SEO Service Delete Successfully.');
    }



   // ---------------- Palns Section --------------------

    public function plan()
    {
        $plans = Plan::all();
        return view('admin.plan.plans',compact('plans'));
    }

    public function newPlan()
    {
        return view('admin.plan.plansNew');
    }

    public function newPostPlan(Request $request)
    {
        $this->validate($request,[
            'description' => 'required',
        ]);

        $plan = new Plan;
        $plan->plan = $request['description'];
        $plan->save();
        return redirect()->to(route('adminPlan'))->with('success','Plan Add Successfully.');
    }

    public function editPlan(Plan $plan)
    {
        return view('admin.plan.planEdit',compact('plan'));
    }

    public function editPostPlan(Request $request,$id)
    {
        $this->validate($request,[
            'description' => 'required',
        ]);

        $plan = Plan::findorFail($id);
        $plan->plan = $request['description'];
        $plan->save();
        return redirect()->to(route('adminPlan'))->with('success','Plan Edit Successfully.');
    }

    public function deletePlan($id)
    {
        $plan = Plan::findorFail($id);
        if($plan)
        {
            $plan->delete();
        }
        return redirect()->to(route('adminPlan'))->with('success','Plan Delete Successfully.');
    }



// ---------------- Category Section --------------------

    

    public function category()
    {
        $categories = Category::all();
        return view('admin.category.categorys',compact('categories'));
    }

    public function newCategory()
    {
        return view('admin.category.categoryNew');
    }

    public function newPostCategory(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|string'
        ]);
        $category = new Category;
        $category->name = $request['title'];
        $category->save(); 
        return redirect()->to(route('adminCategory'))->with('success','Category Add Successfully.');
    }

    public function editCategory(Category $category)
    {
        return view('admin.category.categoryEdit',compact('category'));
    }

    public function editPostCategory(Request $request, $id)
    {
         $this->validate($request,[
            'title' => 'required|string'
        ]);
        $category = Category::findorFail($id);
        $category->name = $request['title'];
        $category->save(); 
        return redirect()->to(route('adminCategory'))->with('success','Category Edit Successfully.');
    }

    public function deleteCategory($id)
    {
        $category = Category::findorFail($id);
        $category->delete();
        return redirect()->to(route('adminCategory'))->with('success','Category Delete Successfully.');
    }




    // ---------------- Blog Section --------------------


    public function blog()
    {
        $blogs = Blog::all();
        return view('admin.blog.blogs',compact('blogs'));
    }

    public function newBlog()
    {
        $categories = Category::all();
        return view('admin.blog.blogsNew',compact('categories'));
    }

    public function newPostBlog(BlogRequest $request)
    {
        $blog = New Blog;
        $blog->category_id = $request['category'];
        $blog->user_id = Auth::id();
        $blog->title = $request['title'];
        $blog->description = $request['description'];

        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        $thumbnail->move('public/blog-image', $filename);
        $blog->thumbnail = $filename;
        $blog->save(); 
        return redirect()->to(route('adminBlog'))->with('success','Blog Add Successfully.');
    }

    public function editBlog(Blog $blog)
    {
        $categories = Category::all();
        return view('admin.blog.blogEdit',compact('blog','categories'));
    }

    public function editPostBlog(Request $request,$id)
    {
        $this->validate($request,[
            'category' => 'required|integer',
            'title' => 'required|string',
            // 'title' => 'required|string',
            'description' => 'required',
             ]);

        $blog = Blog::findorFail($id); 
        $blog->category_id = $request['category'];
        $blog->user_id = Auth::id();
        $blog->title = $request['title'];
        $blog->description = $request['description'];
        $old_image = $request['thumbnail_old'];

        if($request->hasFile('thumbnail'))
        {
        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        if($thumbnail->move('public/blog-image', $filename))
        {
            @unlink('public/blog-image/'.$old_image);
        }
        $blog->thumbnail = $filename;
        }
         $blog->save(); 
         return redirect()->to(route('adminBlog'))->with('success','Blog Edit Successfully.');
    }

    public function deleteBlog($id)
    {
        $blog = Blog::findorFail($id);
        if($blog){
            @unlink('public/blog-image/'.$blog->thumbnail);
            $blog->delete();
        }
        return redirect()->to(route('adminBlog'))->with('success','Blog Delete Successfully.');
    }




    // ---------------- Socail Links Section --------------------


    public function links()
    {
        $links = SocialLink::all();
        return view('admin.links.link',compact('links'));
    }

    public function newLinks()
    {
       return view('admin.links.linkNew');
    }

    public function newPostLinks(Request $request)
    {
        if($request['facebook'] == "" && $request['twitter'] == "" && $request['google'] == "" && $request['instagram'] == "" && $request['youtube'] == "")
      {
        return back()->with('error','Atleast One Socail Link will be required.');
      }

      $this->validate($request,[
        'facebook' => 'max:255',
        'twitter' => 'max:255',
        'google' => 'max:255',
        'instagram' => 'max:255',
        'youtube' => 'max:255',
      ]);

      $links = new SocialLink;

      if($request['facebook'] != "")
      { $links->facebook = $request['facebook']; }
      if($request['twitter'] != "")
      { $links->twitter = $request['twitter']; } 
      if($request['google'] != "")
      { $links->google = $request['google']; }
      if($request['instagram'] != "")
      { $links->instagram = $request['instagram']; }
      if($request['youtube'] != "")
      { $links->youtube = $request['youtube']; }

        $links->save();
        return redirect()->to(route('adminLink'))->with('success','Links Add Successfully.');
    }

    public function editLinks(SocialLink $link)
    {
      return view('admin.links.linkEdit',compact('link'));
    }

    public function editPostLinks(Request $request, $id)
    {
        
       if($request['facebook'] == "" && $request['twitter'] == "" && $request['google'] == "" && $request['instagram'] == "" && $request['youtube'] == "")
      {
        return back()->with('error','Atleast One Socail Link will be required.');
      }

      $this->validate($request,[
        'facebook' => 'max:255',
        'twitter' => 'max:255',
        'google' => 'max:255',
        'instagram' => 'max:255',
        'youtube' => 'max:255',
      ]);

      $links = SocialLink::findorFail($id);
      $links->facebook = $request['facebook'];
      $links->twitter = $request['twitter'];
      $links->google = $request['google'];
      $links->instagram = $request['instagram'];
      $links->youtube = $request['youtube'];
      $links->save();
      return redirect()->to(route('adminLink'))->with('success','Links Edit Successfully.');

    }

    public function deleteLinks($id)
    {
      $links = SocialLink::findorFail($id);
      if($links)
      {
        $links->delete();
      }
      return redirect()->to(route('adminLink'))->with('success','Links Delete Successfully.');
    }
    

    // ---------------- Address Section --------------------

   

    public function address()
    {
        $address = Address::all();
        $numbers = Number::all();
        $emails = Email::all();
        return view('admin.address.address',compact('address','numbers','emails'));
    }

    public function newAddress()
    {
        return view('admin.address.addressNew');
    }

    public function newPostAddress(Request $request)
    {
        $this->validate($request,[
            'address' => 'required|string',
        ]);

        $address = New Address;
        $address->address = $request['address'];
        $address->save();
        return redirect()->to(route('adminAddress'))->with('success','Address Add Successfully.');
    }

    public function deleteAddress($id)
    {
        $address = Address::findorFail($id);
        if($address)
        {
           $address->delete(); 
        }
        return redirect()->to(route('adminAddress'))->with('success','Address Delete Successfully.');
    }

    public function phone()
    {
         return view('admin.address.phoneNew');
    }

    public function newPostPhone(Request $request)
    {
         $this->validate($request,[
            'number' => 'required|string',
        ]);

        $number = New Number;
        $number->number = $request['number'];
        $number->save();
        return redirect()->to(route('adminAddress'))->with('success','Phone Add Successfully.');
    }

    public function deletePhone($id)
    {
       $phone = Number::findorFail($id);
       if($phone)
       {
         $phone->delete();
       }

       return redirect()->to(route('adminAddress'))->with('success','Phone Delete Successfully.');
    }

    public function email()
    {
        return view('admin.address.emailNew');
    }

    public function newPostEmail(Request $request)
    {
      $this->validate($request,[
            'email' => 'required|email|unique:emails',
        ]);

        $email = New Email;
        $email->email = $request['email'];
        $email->save();
        return redirect()->to(route('adminAddress'))->with('success','Email Address Add Successfully.');   
    }

    public function deleteEmail($id)
    {
       $email = Email::findorFail($id);
       if($email)
       {
         $email->delete();
       }

       return redirect()->to(route('adminAddress'))->with('success','Email Address Delete Successfully.');
    }


//-------------------------------- Products ---------------------------------

    public function products()
    {
      $products = Product::all();
      return view('admin.products.product',compact('products'));  
    }    

    public function newProduct()
    {
        return view('admin.products.newProduct');
    }

    public function newPostProduct(Request $request)
    {
         $this->validate($request,[
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'stock' => 'required|string',
            'price' => 'required|string',
            'thumbnail' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if($request->hasFile('thumbnail'))
        {
        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        if($thumbnail->move('public/Product', $filename))
        {
           $product = new Product();
           $product->title = $request->title;
           $product->description = $request->description;
           $product->in_stock = $request->stock;
           $product->price = $request->price;
           $product->thumbnail = $filename;
           $product->save();
        }
        return redirect()->to(route('adminProducts'))->with('success','Product Add Successfully.');
        }else{

        return back()->with('errors' , 'There was something wrong.');
        }
    }

    public function editProduct(Product $product)
    {
         return view('admin.products.editProduct',compact('product'));
    }

    public function editPostProduct(Request $request,$id)
    {

         $this->validate($request,[
            'title' => 'required|string|max:100',
            'description' => 'required|string',
            'stock' => 'required|string',
            'price' => 'required|string',
        ]);

        $product = Product::findorFail($id); 
        $product->title = $request->title;
        $product->description = $request->description;
        $product->in_stock = $request->stock;
        $product->price = $request->price;
        $old_image = $request['thumbnail_old'];

        if($request->hasFile('thumbnail'))
        {
        $thumbnail = $request->file('thumbnail');
        $filename = date('his').$thumbnail->getClientOriginalName();
        if($thumbnail->move('public/product', $filename))
        {
            @unlink('public/product/'.$old_image);
        }
        $product->thumbnail = $filename;
        }
        if($product->save())
        {
        return redirect()->to(route('adminProducts'))->with('success','Product Update Successfully.');
        }else{
        return back()->with('errors' , 'There was something wrong.');
        }

    }

    public function deleteProduct($id)
    {
        $product = Product::findorFail($id);
        if($product->delete())
        {
            @unlink('public/product/'.$product->thumbnail);
            return redirect()->back()->with('success','Product Delete Successfully.');
        }
    }


//--------------------------------- Orders ----------------------------------

    public function orders()
    {
        $orders = DeliveryAddress::all();
        return view('admin.orders.order',compact('orders'));
    }

    public function viewOrder($id)
    {
        $orders = Order::where('order_id',$id)->get();
        $address = DeliveryAddress::findorFail($id);
        $head = '<strong>Date: </strong>'.date("d/m/Y", strtotime($address->created_at)).'<br><br><div class="row"><div class="col-md-6"><strong>To,</strong><br>'.$address->name.',<br>'.$address->address.',<br>'.$address->state.'<br></div></div>';
        $total = [];
        $data = "";
        $i=1;
        foreach($orders as $order)
        {
            $data .= "<tr>
            <td>".$i++."</td>
            <td><img src='../public/Product/".$order->product->thumbnail."' height='50' width='50'></td>
            <td>".$order->product->title."</td>
            <td>".$order->qty."</td>
            </tr>";
            $total[] .= $order->product->price*$order->qty;
        }

        $data.= '<tr>
        <td colspan="3" style="text-align:right;"><b>Tax:</b></td>
        <td style="font-weight:500"><b>&#8377;</b>0</td>
        </tr><tr>
        <td colspan="3" style="text-align:right;"><b>Subtotal:</b></td>
        <td style="font-weight:500"><b>&#8377;</b>'.array_sum($total).'</td>
        </tr>';
        return response()->json(['data' => $data, 'head' =>$head]);
    }

    public function editOrder(DeliveryAddress $order)
    {
        return view('admin.orders.editOrder',compact('order'));
    }

    public function editOrderPost(Request $request,$id)
    {
        $this->validate($request,[
        'name' => 'required|string|max:255',
        'email' => 'required|email',
        'address' => 'required|string',
        'city' => 'required|string',
        'state' => 'required',
        'zip'  => 'required',
        'status' => 'required'
        ]);
        
            $address = DeliveryAddress::findorFail($id);
            $address->name = $request->name;
            $address->email = $request->email;
            $address->address = $request->address;
            $address->city = $request->city;
            $address->state = $request->state;
            $address->zip = $request->zip;
            $address->status = $request->status;
            if($address->save()){
                return redirect()->route('adminOrders')->with('success','Order Edit Successfully.');
            }
        
    }
       
    public function deleteOrder($id)
    {
        $address = DeliveryAddress::findorFail($id);
        if($address->delete())
        {
            $order = Order::where('order_id',$id)->get();
            $order->delete();
            return redirect()->route('adminOrders')->with('success','Order Delete Successfully.');
        } 
    }
}
