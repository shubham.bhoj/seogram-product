<?php

namespace App\Http\Requests;

use App\Rules\CommentOverlap;
use Illuminate\Foundation\Http\FormRequest;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'blog_id' => 'required',
            'name' => 'required|string',
            'email' => ['required','email',new CommentOverlap()],
            'msg' => 'required|string',
        ];
    }
}
